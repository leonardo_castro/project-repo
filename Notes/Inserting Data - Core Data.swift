

// Get the ManagedObjectContext from the AppDelegate.
let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
let managedObjectContext = appDelegate.managedObjectContext


// CREATE BIBLE OBJECTS ******************************************************************
// Create a Bible entity subclass object.
let king_james_version = NSEntityDescription.insertNewObjectForEntityForName("Bible", inManagedObjectContext: managedObjectContext) as! Bible

// Set attributes for the object.
king_james_version.version = "King James Bible"

// Save the entity.
do {
    try king_james_version.managedObjectContext?.save()
    print("KJV Bible saved")
} catch {
    print(error)
}



// CREATE BOOK OBJECTS *******************************************************************
let genesis = NSEntityDescription.insertNewObjectForEntityForName("Book", inManagedObjectContext: managedObjectContext) as! Book
// Set attributes for Genesis Book.
genesis.setValue("Genesis", forKey: "title")
genesis.setValue("1", forKey: "number")
genesis.setValue("Old Testament", forKey: "section")
genesis.setValue(king_james_version, forKey: "Bible")

// OR ...
let exodus = NSEntityDescription.insertNewObjectForEntityForName("Book", inManagedObjectContext: managedObjectContext) as! Book
// Set attributes for Genesis Book.
exodus.title = "Exodus"
exodus.number = "2"
exodus.section = "Old Testament"
exodus.bible = king_james_version


// SAVE BOOK ENTITIES.
do {
    try genesis.managedObjectContext?.save()
    print("Genesis Book saved")
} catch {
    print(error)
}
