func retriveDatabaseInfo() {
    
    // Initialize Fetch Request.
    let fetchRequest = NSFetchRequest()
    
    // Create Entity Description
    let entityDescription = NSEntityDescription.entityForName("Bible", inManagedObjectContext: managedObjectContext)
    
    
    // Configure Fetch Request
    fetchRequest.entity = entityDescription
    do {
        
        let result = try managedObjectContext.executeFetchRequest(fetchRequest)
        //print(result)
        
        if (result.count > 0) {
            let KJV = result[0] as! Bible
            let ASV = result[1] as! Bible
            let Darby = result[2] as! Bible
            let YLT = result[3] as! Bible
            let WEB = result[4] as! Bible
            
            print(KJV.version)
            print(ASV.version)
            print(Darby.version)
            print(YLT)
            print(WEB.version)
            
        }
    } catch {
        let fetchError = error as NSError
        print(fetchError)
    }
}

