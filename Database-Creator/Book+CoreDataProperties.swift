//
//  Book+CoreDataProperties.swift
//  Database-Creator
//
//  Created by Leo on 4/10/16.
//  Copyright © 2016 Leo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Book {

    @NSManaged var title: String?
    @NSManaged var number: NSNumber?
    @NSManaged var bible: Bible?
    @NSManaged var chapters: NSOrderedSet?

}
