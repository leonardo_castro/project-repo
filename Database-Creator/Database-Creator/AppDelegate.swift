//
//  AppDelegate.swift
//  Database-Creator
//
//  Created by Leo on 4/10/16.
//  Copyright © 2016 Leo. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
       
        // Override point for customization after application launch.
        // print(applicationDocumentsDirectory.path)
        
        retriveDatabaseInfo()
        

        return true
    }
    
    // MARK: - Core Data Saving support
    
    func createBible() {
    
        // Create a Bible entity subclass object.
        let ASV = NSEntityDescription.insertNewObjectForEntityForName("Bible", inManagedObjectContext: managedObjectContext) as! Bible
        
        // Set attributes for the object.
        ASV.version = "American Standard Version"
        
        // Save the entity.
        do {
            try ASV.managedObjectContext?.save()
            print("ASV Bible Saved")
        } catch {
            print(error)
        }

    }
    
    func retriveDatabaseInfo() {
    
        // Temp Bible Variables.
        var KJV : Bible
        var ASV : Bible
        var Darby : Bible
        var YLT : Bible
        var WEB : Bible
        
        // Initialize Fetch Request.
        let fetchRequest = NSFetchRequest()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entityForName("Bible", inManagedObjectContext: managedObjectContext)

        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        do {
            
            let result = try managedObjectContext.executeFetchRequest(fetchRequest)
            //print(result)

            if (result.count > 0) {
                KJV = result[0] as! Bible
                ASV = result[1] as! Bible
                Darby = result[2] as! Bible
                YLT = result[3] as! Bible
                WEB = result[4] as! Bible
                
                createBooks(KJV)
                
                print(KJV.version!)
                print(ASV.version!)
                print(Darby.version!)
                print(YLT.version!)
                print(WEB.version!)
            
            }
            } catch {
                let fetchError = error as NSError
                print(fetchError)
            }
    

        
    
    }
    
    
    func createBooks(bible: Bible) {
        // Create a Book entity subclass object.
        let genesis = NSEntityDescription.insertNewObjectForEntityForName("Book", inManagedObjectContext: managedObjectContext) as! Book
        genesis.bible = bible
        genesis.number = 1
        genesis.title = "Genesis"
        
        // Create a Book entity subclass object.
        let exodus = NSEntityDescription.insertNewObjectForEntityForName("Book", inManagedObjectContext: managedObjectContext) as! Book
        exodus.bible = bible
        exodus.number = 2
        exodus.title = "Exodus"
        
        // Create a Book entity subclass object.
        let leviticus = NSEntityDescription.insertNewObjectForEntityForName("Book", inManagedObjectContext: managedObjectContext) as! Book
        leviticus.bible = bible
        leviticus.number = 3
        leviticus.title = "Leviticus"
        
        // Create a Book entity subclass object.
        let numbers = NSEntityDescription.insertNewObjectForEntityForName("Book", inManagedObjectContext: managedObjectContext) as! Book
        numbers.bible = bible
        numbers.number = 4
        numbers.title = "Numbers"
        
        // Save the entity.
        do {
            try genesis.managedObjectContext?.save()
            try exodus.managedObjectContext?.save()
            try leviticus.managedObjectContext?.save()
            try numbers.managedObjectContext?.save()
            
        } catch {
            print(error)
        }
        
    }
    
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.absoluto.Database_Creator" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Database_Creator", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}

