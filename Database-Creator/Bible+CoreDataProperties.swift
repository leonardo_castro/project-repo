//
//  Bible+CoreDataProperties.swift
//  Database-Creator
//
//  Created by Leo on 4/10/16.
//  Copyright © 2016 Leo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Bible {

    @NSManaged var version: String?
    @NSManaged var books: NSOrderedSet?

}
